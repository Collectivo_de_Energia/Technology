# Smart Power Assistant

## Introduction

Overview into general concept and practical example for installed components

## concept

### power dimming 

https://www.youtube.com/watch?v=jS9ANqJf-ZY&t=6s

### electrical load management

### Maximum Demand Controll

### load shifting

Load Shifting vs Peak Shaving

It is important to differentiate between load shifting and peak shaving which is another demand side management technique used for optimizing energy consumption. These concepts are often used synonymously but there are slight differences between them. Peak shaving is a strategy for avoiding peak demand charges in the electrical grid by quickly reducing power consumption during intervals of high demand through renewable energy sources or on-site power generation systems. Load shifting, however, simply refers to the shifting of electricity consumption from the electrical grid to a different time interval. Thus, during load shifting overall electricity consumption remains constant. Differences aside, load shifting, and peak shaving have something critical in common; the benefits of both tactics are amplified when coupled with Battery Energy Storage Systems.[1]

[1] https://www.exro.com/industry-insights/load-shifting

## device example

### voltronic

#### Software

https://voltronicpower.com/en-US/Support/Software