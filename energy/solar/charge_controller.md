## Epever - MPPT Solar Controller Dual Battery 30A 12v / 24v DR 3210N

[shop](https://en.wccsolar.net/product-page/regulador-solar-mppt-bater%C3%ADa-dual-30a-12v-24v-dr-3210n)
[data sheet](https://www.mpptsolar.com/es/pdf/reguladores/manual-epever-duoracer.pdf)

    Brand: Temank
    Product Model: DR3210N
    Technology: MPPT
    Maximum Power Point Tracking technology with ultra-fast tracking speed and the tracking efficiency is no less than 99.5% guaranteed
    Advanced MPPT control algorithm to minimize the MPPlost rate and lost time
    The wider range of the MPP operation voltage to improve the PV module utilization
    Auto control function of charging power & charging current limitation(BATT1)
    High quality and low failure rate components of ST, TI and Infineon to ensure the product life
    Digital circuit control of adaptive three-stage charging mode to enhance BATT1 life.
    BATT1 type can be set via LED/LCD.
    Product runs into the low-power mode when there is no manual operation for a long time, and charging conditions are not satisfied (PV<5V).
    100% charging and discharging in operationenvironmental temperature range.
    LED and LCD dispiay units selectable.
    AES control signal for car refrigerator to avoid energy waste.
    Standard Modbus protocol, and RS485 (5V/200mA) communication port for the customer to expand the application area.
