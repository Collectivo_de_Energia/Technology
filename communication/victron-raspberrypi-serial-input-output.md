# victron-raspberrypi-serial-input-output

## CCGX Hardware and software introduction

### hardware

 The CCGX is based on the Technexion board TAM-3517, which is using the Texas Instruments Sitara AM3517, containing a ARM Cortex-A8 microprocessor: 

tx 

rx

5v

gnd

## software

Linux kernel & OS

The software platform of the CCGX is called Venus: Victron Energy Unix like distro with a linux kernel. It is based on and built with OpenEmbedded, a build framework for embedded Linux. It has all kinds of tools, mechanisms and recipes to build the Linux kernel and make a full embedded Linux rootfs. Maintained versions are listed here: https://wiki.yoctoproject.org/wiki/Releases.

### venus OS

To build the Venus yourself, see: (venus)[https://github.com/victronenergy/venus]

Victron software

we are using a mixture of C, C++, QML (for the user interface) and Python. As a main data exchange, to share values such as voltages, as well as settings and other data, we use D-Bus. D-bus is for inter process communication, see google and the (D-Bus page on wikipedia)[http://en.wikipedia.org/wiki/D-Bus] for more information. This diagram gives a good overview of the whole thing: 

#### adavise forums

(forum)[https://community.victronenergy.com/questions/23853/connect-raspberry-pi-directly-to-mppt-over-serial.html]

##### jkario answered · Jul 20 2019 at 9:01 PM Best AnswerACCEPTED

MPPTs use 5V and the Pi GPIO pins are 3v3 so you cannot connect them directly. You would need a logic level converter in between. You could use one of those cheap eBay USB to Serial TTL cables and make your own cable by cutting the jumper cables and replacing them with a 4pin JST-PH connector. Be sure to only connect GND, TX and RX. You should never ever connect together the USB port 5V with the VE.Direct port 5V. Also, the cable is not isolated. I presume the official Victron cable has a SI8422 chip inside to provide isolation (just a guess). I would use the official cable. Given that it's going to take time to source the components and solder the connector and the end result is not as good as the original (no isolation) it's just not worth the effort. Choose your battles.


(venus wiki)[https://github.com/victronenergy/venus/wiki]

(MORE Infromation)[https://www.victronenergy.com/live/open_source:ccgx:start]
(technical information victron)[https://www.victronenergy.com/support-and-downloads/technical-information]


(raspberrypi-install-venus-image)[https://github.com/victronenergy/venus/wiki/raspberrypi-install-venus-image]

