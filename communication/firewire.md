## FIREWIRE

https://www.kernel.org/doc/html/latest/driver-api/firewire.html

## software


The FFADO project aims to provide a generic, open-source solution for the support of FireWire based audio devices for the Linux platform. It is the successor of the FreeBoB project.
http://ffado.org/